import { Component } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { MovieSearchService } from '../services/movie-search/movie-search.service';
import { Movie } from './movie';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
 
  search: string = null;
  loading = false;

  constructor(private route: ActivatedRoute, private router: Router) { 
    router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        let page = data.url.split('/')[1];
        
        if (page.length <= 0 || page === 'detail') {
          this.search = null;
        } else if (page === 'search') {
          this.search = data.url.split('/')[2].split('?')[0];
        }
      }
    });
   }

  onSearch(value: string) {
    if (!value || value.length <= 0) {
      return;
    }

    this.router.navigate(['/search', value]);
  }

}
