import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

@Injectable()
export class MovieSearchService {

  constructor(private http: HttpClient) { }

  getById(id: string) : Observable<any> {
    return this.http.get(`${environment.apiBaseUrl}i=${id}&plot=full`);
  }

  geAllByTitle(title: string, page: number) : Observable<any> {
    return this.http.get(`${environment.apiBaseUrl}s=${title}&page=${page}`);
  }
}
