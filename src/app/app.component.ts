import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  bgImageStyle: any;

  constructor(private router: Router) {
    router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        let page = data.url.split('/')[1];
        if (page === 'search') {
          this.bgImageStyle = {
            'background-image': 'url("/assets/backgroundImg2.jpg")'
          };
        } else if (page === 'detail') {
          this.bgImageStyle = {
            'background-image': 'url("/assets/backgroundImg4.jpg")'
          };
        } else {
          this.bgImageStyle = {
            'background-image': 'url("/assets/backgroundImg.jpg")'
          };
        }
      }
    });
  }
}
