export class Movie {
    Poster: string;
    Title: string;
    Type: string;
    Year: string;
    imdbID: string;
}

export class MovieDetail extends Movie {
      Rated: string;
      Released: string;
      Runtime: string;
      Genre: string;
      Director: string;
      Writer: string;
      Actors: string;
      Plot: string;
      Language: string;
      Country: string
      Awards:string;
      Ratings: Array<object>;
      Metascore: string
      imdbRating: string;
      imdbVotes: string;
      DVD: string;
      BoxOffice: string;
      Production: string;
      Website: string;
      Response: string;
}