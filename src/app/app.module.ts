import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, NgForm, NgModel, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';


import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { MovieSearchService } from './services/movie-search/movie-search.service';
import { ResultComponent } from './search/result/result.component';
import { DetailComponent } from './search/detail/detail.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ResultComponent,
    DetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    NgxPaginationModule
  ],
  providers: [
    NgForm,
    NgModel,
    MovieSearchService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
