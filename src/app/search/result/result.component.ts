import { Component } from '@angular/core';
import { MovieSearchService } from '../../services/movie-search/movie-search.service';
import { Movie } from '../movie';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent {

  page = 1;
  searchQuery: string = null;
  loading = false;
  movies: Array<Movie> = [];
  moviesTotal: number;
  error: string;
  notSearched = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private movieService: MovieSearchService
  ) {
    // Subscribe to router change and query imdb API with title and page number if specified.
    router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.searchQuery = this.route.snapshot.params['title'] || null;
        this.page = this.route.snapshot.queryParams['page'] || 1;

        if (this.searchQuery) {
          this.onSearch(this.searchQuery, this.page);
        }
      }
    });
  }


  viewMovieDetail(movie: Movie) {
    // Navigate to the detail page using the movie id
    this.router.navigate(['/detail', movie.imdbID], {queryParams: {returnUrl: this.router.url}});
  }

  onPaging(value: string, page: number) {
    // Handle paging by updating the browser url and making api call to get data for next page.
    this.router.navigate(['/search', value], {queryParams: {page: page}});
    this.onSearch(value, page);
  }

  onSearch(value: string, page?: number) {
    if (!value || value.length <= 0) {
      return;
    }

    let currentPage = page || 1;

    this.loading = true;
    this.error = null;

    this.movieService.geAllByTitle(value, currentPage)
      .subscribe((response: any) => {
        this.loading = false;
        this.notSearched = false;
        if (response.Response === 'True') {
          this.movies = response.Search;
          this.moviesTotal = response.totalResults
        } else if (response.Response === 'False') {
          this.error = response.Error;
          this.movies = [];
        }
      }, error => {
        this.loading = false;
        this.notSearched = false;
        this.error = error;
      });
  }
}
