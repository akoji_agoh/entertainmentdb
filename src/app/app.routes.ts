import { Routes } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { ResultComponent } from './search/result/result.component';
import { DetailComponent } from './search/detail/detail.component';

export const appRoutes: Routes = [
    { 
        path: '', 
        component: SearchComponent,
        children: [
            { path: 'search/:title', component: ResultComponent },
            { path: 'detail/:id', component: DetailComponent }
        ]
    },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];