import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieSearchService } from '../../services/movie-search/movie-search.service';
import { MovieDetail } from '../movie';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  imdbID: string;
  loading = false;
  error: string;
  movie: MovieDetail;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private movieService: MovieSearchService) { }

  ngOnInit() {
    this.imdbID = this.route.snapshot.params['id'] || null;
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || null;

    // Get item if an imdbID is passed in the url.
    if (this.imdbID) {
      this.getMovie();
    }
  }

  getMovie() {
    // Make api call to get data from imdb API endpoint.
    this.loading = true;
    this.movieService.getById(this.imdbID)
      .subscribe(response => {
        this.loading = false;
        if (response.Response === 'True') {
          this.movie = response;
        } else if (response.Response === 'False') {
          this.error = response.Error;
        }
      }, error => {
        this.loading = false;
        this.error = error;
      });
  }

  goBackToSearch() {
    if (!this.returnUrl) {
      return;
    }

    // If return url is passed in use it to navigate back to the search page.
    this.router.navigateByUrl(this.returnUrl);
  }
}
